const _ = require('lodash');
const assert = require('assert');
const debug = require('debug')('faker:upgrade_gateway');

module.exports =  fpm => {

    const router = fpm.createRouter();

    router.post('/upg/queryUpgTours', async ctx => {
        try {          
          const body = ctx.request.body;
          const {
            version = "30",
            channelCode = "B2C",
            userNo = "10001",
            ip = '127.0.0.1',
            request = {}, 
          } = body;

          const {
            certNo,
            psgName,
          } = request;

          debug('Request Info: %O', request);
          ctx.body = {
            version,
            channelCode,
            userNo,
            servieCode: "-1",
            resultCode: "1001",
            errorMsg: "OK",
            result: [
                {
                    flightNo: "HO1073",
                    flightDate: "2019-10-07",
                    depAirportCode: "SHA",
                    arrAirportCode: "WUH",
                    depTerminal: "浦东T2",
                    arrTerminal: "天河T3",
                    depTime: "07:55",
                    arrTime: "10:05",
                    planeType: "空客A321(中)",
                    psgName: psgName,
                    tktNo: "0181156006646",
                    pnrNo: "",
                    certNo: certNo,
                    originCabin: "Y",
                    orginSeatNo: "D1",
                    cabin: "A",
                    seatNo: "A1",
                    type: "UPG",
                    status: "FRO",
                    orderNo: "ORD19080900024708",
                    price: "500.00",
                    currency: "CNY",
                    remark: "--",
                    canUpgrade: true,
                    unmanageableReason: "--",
                }
            ]

          }
          
        } catch (error) {
          ctx.body = {
            resultCode: "10001",
            errorMsg: error.message
          }
        }
      })


      router.post('/upg/queryUpgSeatMap', async ctx => {
        try {          
          const body = ctx.request.body;
          const {
            version = "30",
            channelCode = "B2C",
            userNo = "10001",
            ip = '127.0.0.1',
            request = {}, 
          } = body;

          const {
            flightNo,
            flightDate,
            depAirportCode,
            arrAirportCode,
            currency = 'CNY',
          } = request;

          debug('Request Info: %O', request);
          ctx.body = {
            version,
            channelCode,
            userNo,
            servieCode: "-1",
            resultCode: "1001",
            errorMsg: "OK",
            result: {
                flightNo,
                cabin: 'A',
                flightDate,
                depAirportCode,
                arrAirportCode,
                planeType: '空客A321(中)',
                seatMaplist: [
                    {
                        seatColumn: 'A',
                        seatRow: 1,
                        seatStatus: "*",
                        side: "L",
                        cabin: 'A',
                        price: "500.00",
                        currency
                    }
                ]
            }

          }
          
        } catch (error) {
          ctx.body = {
            resultCode: "10001",
            errorMsg: error.message
          }
        }
      })

      router.post('/upg/passengerUpgrade', async ctx => {
        try {          
          const body = ctx.request.body;
          const {
            version = "30",
            channelCode = "B2C",
            userNo = "10001",
            ip = '127.0.0.1',
            request = {}, 
          } = body;

          const {
            flightNo,
            flightDate,
            depAirportCode,
            arrAirportCode,
            psgName,
            phoneNo,
            ffpId,
            ffpCardNo,
            certNo,
            tktNo,
            pnrNo,
            cabin,
            seatNo,
            originCabin,
            originSeatNo,
            orderNo,
            price,
            currency = 'CNY',
            upgType
          } = request;

          debug('Request Info: %O', request);
          ctx.body = {
            version,
            channelCode,
            userNo,
            servieCode: "-1",
            resultCode: "1001",
            errorMsg: "OK",
            result: {
                flightNo,
                cabin: 'A',
                flightDate,
                depAirportCode,
                arrAirportCode,
                depTerminal: "浦东T2",
                arrTerminal: "天河T3",
                depTime: "07:55",
                arrTime: "10:05",
                planeType: "空客A321(中)",
                psgName: psgName,
                tktNo: "0181156006646",
                pnrNo: "",
                certNo: certNo,
                originCabin: "Y",
                orginSeatNo: "D1",
                cabin: "A",
                seatNo: "A1",
                type: "UPG",
                status: "T",
                orderNo: "ORD19080900024708",
                price: "500.00",
                currency: "CNY",
                remark: "--",
            }

          }
          
        } catch (error) {
          ctx.body = {
            resultCode: "10001",
            errorMsg: error.message
          }
        }
      })

    fpm.bindRouter(router);
}