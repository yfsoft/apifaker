FROM node:12-alpine as builder

COPY package.json /app/package.json

RUN cd /app && \
    npm i --production  --registry https://registry.npm.taobao.org

FROM node:12-alpine

COPY --from=builder /app/node_modules /app/node_modules

COPY source /app/source
COPY mockdata /app/mockdata
COPY package.json /app/package.json

WORKDIR /app

EXPOSE 9999

ENTRYPOINT ["node"]

CMD ["source/app.js"]