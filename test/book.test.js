const assert = require('assert');
const debug = require('debug')('TEST');
const axios = require('axios');

axios.defaults.baseURL = 'http://172.22.0.20:6121';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

describe('Basic Service Test Unit', function(){
  before(function(done){
    done();
  })

  it('/PublicQueryFlightFareV30()', async() => {
    try {
      const rsp = await axios.post('/PublicQueryFlightFareV30', {
        "Version": "10",
        "ChannelCode": "MOBILE",
        "UserNo": "10007",
        "RouteType": "OW",
        "CurrencyCode": "CNY",
        "LangCode": "CN",
        "SegCondList": [
          {
            "SegNO": 0,
            "FlightDirection": "G",
            "DepCity": "SZX",
            "ArrCity": "KOS",
            "FlightDate": "2019-08-10"
          }
        ],
        "ReadRedis": "N"
      });
      const { status, data } = rsp;
      debug(rsp);
      assert(status === 200, 'http status should be 200')
      assert(data.errcode === 0, 'errcode should be 0')
      
    } catch (error) {
      throw error;
    }
  })
  
})