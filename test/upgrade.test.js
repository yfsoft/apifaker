const assert = require('assert');
const _ = require('lodash');
const debug = require('debug')('TEST');
const axios = require('axios');

axios.defaults.baseURL = 'http://172.22.0.12:9210/cuss';

const query = '/upg/queryUpgTours';

const seat = '/upg/queryUpgSeatMap';

const frez = '/upg/passengerUpgrade';


const basicRequest = {
  version: "30",
  channelCode: "30000",
  userNo: "10001",
  ip: "127.0.0.1",
  request: {}
}

const userInfo = {
  "psgName": "王帆",
  "phoneNo": "13770683580",
  "ffpId": 12282297,
  "ffpCardNo": "2762118656",
  "certNo": "321088198801235454",
}

const basicInfo = {
  certNo: '0182363302783',
  psgName: '王帆',
}

/**
 * {
      "flightNo": "HO1111",
      "flightDate": "2019-08-21",
      "depAirportCode": "SHA",
      "arrAirportCode": "SZX",
      "tktNo": "0182363273836",
      "pnrNo": "PWT4V8",
      "cabin": "J",
      "seatNo": "3F",
      "originCabin": "Y",
      "originSeatNo": "15B",
      "orderNo": "ORD19082000025611",
      "price": "727",
      "currency": "CNY",
      "upgType": "UNFREEZE"
   }
 */
const MOCK_DATA = {
  ticket: {},
  seat: {},
  frez: {

  },
  orderNO: '',
}

describe('登机口升舱的 Test Unit', function(){
  before(async () => {
    // 根据票号 ，获取升舱状态，PNR，航班信息，可以升级的具体舱位
    // 创建渠道订单号和订单号

    let rsp, status, data ;
    try {
      rsp = await axios.post(query, _.assign(basicRequest, { request: basicInfo }));
      status = rsp.status;
      data = rsp.data;
      assert(status === 200, 'http status should be 200')
      assert(data.resultCode === '1001', 'errcode should be 1001')
      assert(_.size(data.result) === 1, 'result length should be 1')
      const ticket = data.result[0];
      debug(ticket);
      /**
       *     "arrAirportCode": "SZX",
      "arrTerminal": "T3",
      "arrTime": "1000",
      "basePrice": 777,
      "cabin": "Y",
      "canUpgrade": true,
      "certNo": "0182363273881",
      "currency": "",
      "depAirportCode": "SHA",
      "depTerminal": "T2",
      "depTime": "0735",
      "discountPrice": 727,
      "flightDate": "2019-08-21",
      "flightNo": "HO1111",
      "originCabin": "Y",
      "originSeatNo": "28J",
      "payOrderInfo": null,
      "planeType": "787",
      "pnrNo": "MTCNNY",
      "psgName": "王帆",
      "seatNo": "28J",
      "status": "",
      "supportUpgBusinessCabin": "J",
      "tktNo": "0182363273881",
      "unmanageableReason": ""
       */
      const { canUpgrade, unmanageableReason } = ticket;
      assert(canUpgrade, `这个票${ basicInfo.certNo }不能升舱; 原因 ： ${ unmanageableReason }`);

      MOCK_DATA.ticket = ticket;


      // go get the seat

      const { flightNo, flightDate, depAirportCode, arrAirportCode,tktNo, currency } = MOCK_DATA.ticket;
      rsp = await axios.post(seat, _.assign(basicRequest, { request: _.assign({
        flightNo, flightDate, depAirportCode, arrAirportCode,tktNo, currency:'CNY'
      }, basicInfo) }));
      
      status = rsp.status;
      data = rsp.data;
      // debug(data);
      assert(status === 200, 'http status should be 200')
      assert(data.resultCode === '1001', 'errcode should be 1001')

      result = data.result.seatMaplist;
      // debug(result);
      MOCK_DATA.seat = _.find(result, item => {
        return item.seatStatus === '*'
      });
      assert(MOCK_DATA.seat !== undefined, 'No Seat To Upgrade!');
    } catch (error) {
      throw error;
    }
  })

  after(async () => {
    // 解冻座位
    let rsp, status, data ;    
    
    const { flightNo, flightDate, depAirportCode, arrAirportCode,tktNo, pnrNo, cabin, seatNo, discountPrice, currency  } = MOCK_DATA.ticket;

    try {
      rsp = await axios.post(frez, _.assign(basicRequest, { request: _.assign(basicInfo, userInfo, {
        flightNo, flightDate, depAirportCode, arrAirportCode,tktNo, pnrNo, price: discountPrice, currency: 'CNY',
        originCabin: cabin, originSeatNo: seatNo, 
        orderNo: MOCK_DATA.orderNO,
        cabin: MOCK_DATA.seat.cabin, seatNo: `${MOCK_DATA.seat.seatRow}${MOCK_DATA.seat.seatColumn}`,
        upgType: "UNFREEZE"
      }) }));
      status = rsp.status;
      data = rsp.data;
      debug(data);
      assert(status === 200, 'http status should be 200')
      assert(data.resultCode === '1001', 'errcode should be 1001')
      
    } catch (error) {
      throw error;
    }
  })

  it('冻结座位', async() => {
    let rsp, status, data ;
    const { flightNo, flightDate, depAirportCode, arrAirportCode,tktNo, pnrNo, cabin, seatNo, discountPrice, currency  } = MOCK_DATA.ticket;
    MOCK_DATA.orderNO = 'Faker:' + _.now();
    try {
      rsp = await axios.post(frez, _.assign(basicRequest, { request: _.assign(basicInfo, userInfo, {
        flightNo, flightDate, depAirportCode, arrAirportCode,tktNo, pnrNo, price: discountPrice, currency: 'CNY',
        originCabin: cabin, originSeatNo: seatNo, 
        orderNo: MOCK_DATA.orderNO,
        cabin: MOCK_DATA.seat.cabin, seatNo: `${MOCK_DATA.seat.seatRow}${MOCK_DATA.seat.seatColumn}`,
        upgType: "FREEZE"
      }) }));
      status = rsp.status;
      data = rsp.data;
      debug(data);
      assert(status === 200, 'http status should be 200')
      assert(data.resultCode === '1001', 'errcode should be 1001')
      
    } catch (error) {
      throw error;
    }
  })

})