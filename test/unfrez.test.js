const assert = require('assert');
const _ = require('lodash');
const debug = require('debug')('TEST');
const axios = require('axios');
const path = require('path');
const fs = require('fs');
let data = { env: 'test', ticketNo: '', psgName: '' };

debug(path.join(__dirname,'data.json'));
if(fs.existsSync(path.join(__dirname,'data.json'))){
  data = _.assign(data,  require(path.join(__dirname,'data.json')));
}

debug(data);
const { env, ticketNo, psgName } = data;

const UPG_BASEURL = `http://172.22.0.${ env == 'pre'? '11': '12'}:9210/cuss`;
const DNG_BASEURL = `http://172.22.0.${ env == 'pre'? '11': '12'}:9214/dng`;
const CHANNEL_CODE =  env == 'pre'? '40000': '10000';

const query = '/upg/queryUpgTours';

const seat = '/upg/queryUpgSeatMap';

const frez = '/upg/passengerUpgrade';

const dng = '/passengerDeck';


const basicRequest = {
  version: "30",
  channelCode: CHANNEL_CODE,
  userNo: "10001",
  ip: "127.0.0.1",
  request: {}
}

const userInfo = {
  "psgName": "王帆",
  "phoneNo": "13917199254",
  "ffpId": 3499052,
  "ffpCardNo": "2881380353",
  "certNo": "321088198801235454",
}

const basicInfo = {
  certNo: ticketNo,
  psgName: psgName,
}

const MOCK_DATA = {
  ticket: {},
  seat: {},
  frez: {

  },
  orderNO: '',
}

describe('登机口升舱的 Test Unit', function(){
  before(async () => {
    // 根据票号 ，获取升舱状态
    let rsp, status, data ;
    try {
      rsp = await axios.post(UPG_BASEURL+ query, _.assign(basicRequest, { request: basicInfo }));
      status = rsp.status;
      data = rsp.data;
      assert(status === 200, 'http status should be 200')
      debug(data);
      assert(data.resultCode === '1001', 'errcode should be 1001')
      data.result =  _.filter(data.result, item => {
        if(item.canUpgrade){
          return false;
        }
        return 'T' === item.status || 'FRO' === item.status || (  '' === item.status && '' === item.unmanageableReason);
      })
      assert(_.size(data.result) === 1, '没有需要降舱的订单信息')
      const ticket = data.result[0];
      
     const { canUpgrade, payOrderInfo } = ticket;
     assert(!canUpgrade, '该票号未升舱，不可解冻')
     assert( !!payOrderInfo, '该票号没有升舱订单信息，不可解冻')

      MOCK_DATA.ticket = ticket;

    } catch (error) {
      throw error;
    }
  })

  it('解冻座位', async() => {
    // 解冻座位
    let rsp, status, data ;

    const { flightNo, flightDate, depAirportCode, payOrderInfo, arrAirportCode,tktNo, pnrNo, cabin, seatNo, originCabin, originSeatNo, currency  } = MOCK_DATA.ticket;

    try {
      if( MOCK_DATA.ticket.status === 'T'){
        // 已经升级到公务舱，需要降下来
        const requestBody = _.assign(basicRequest, { request: _.assign(basicInfo, userInfo, {
          depAirportCode, arrAirportCode,
          "dngCabin": originCabin,
          "dngSeatNo": originSeatNo,
          "ffpCardNo":userInfo.ffpCardNo,
          "ffpId": userInfo.ffpId,
          flightNo, flightDate,
          phoneNo: payOrderInfo.phoneNo,
          tktNo, pnrNo,
          "upgCabin":cabin,
          "upgSeatNo": seatNo
        })} );
        debug('公务舱降舱RequestBody： %O', requestBody);
        rsp = await axios.post(DNG_BASEURL+ dng, requestBody)

        status = rsp.status;
        data = rsp.data;
        debug('公务舱降舱： %O', data);
        assert(status === 200, 'http status should be 200')
        assert(data.resultCode === '1001', 'errcode should be 1001')
      }
      
      

      rsp = await axios.post(UPG_BASEURL+ frez, _.assign(basicRequest, { request: _.assign(basicInfo, userInfo, {
        flightNo, flightDate, depAirportCode, arrAirportCode,tktNo, pnrNo, currency,
        originCabin, originSeatNo, 
        orderNo: payOrderInfo.orderNo,
        price: payOrderInfo.price,
        cabin, seatNo,
        upgType: "UNFREEZE"
      }) }));
      status = rsp.status;
      data = rsp.data;
      debug(data);
      assert(status === 200, 'http status should be 200')
      assert(data.resultCode === '1001', 'errcode should be 1001')
      
    } catch (error) {
      throw error;
    }
  })

})