const _ = require('lodash');
const assert = require('assert');
const debug = require('debug')('faker:service_upgrade_gateway');
const SeatMapData = require('../../mockdata/SeatMap.json');
module.exports =  fpm => {

    const router = fpm.createRouter();

    // curl -X POST -d '{}' http://localhost:9999/faker/timeout
    router.post('/faker/timeout', async ctx=> {
      console.log(ctx.request);
      console.log('request in', new Date())
            await new Promise( ( rs, rj ) => {
                setTimeout(() => {
                 rs(1)
                }, 15000)
            })
            console.log('request out', new Date())
            ctx.body = { errno: 0 };
    });

    router.post('/faker/GateUpgrade/QueryUpgTours/v10', async ctx => {
        try {          
          const body = ctx.request.body;
          const {
            Version = "30",
            ChannelCode = "B2C",
            UserNo = "10001",
            Ip = '127.0.0.1',
            Request = {}, 
          } = body;

          const {
            CertNo,
            PsgName,
          } = Request;

          debug('Request Info: %O', Request);
          ctx.body = {
            Version,
            ChannelCode,
            UserNo,
            ServieCode: "-1",
            ResultCode: "1001",
            ErrorMsg: "OK",
            Result: [
                {
                    Flight: {
                        FlightNo: "HO1073",
                        FlightDate: "2019-10-07",
                        PlaneType: "空客A321(中)",
                        Currency: "CNY",
                        Depart: {
                            AirportCode: "SHA",
                            City: "上海",
                            Terminal: "浦东T2",
                            Time: "07:55"
                        },
                        Arrive: {
                            AirportCode: "WUH",
                            City: "武汉",
                            Terminal: "天河T3",
                            Time: "10:05"
                        }
                    },
                    Passenger:{
                        PsgName, // 姓名
                        CertNo, //ID
                        TktNo: "0181156006646", //票号
                        PnrNo: "pnr" // pnrno
                    },
                    Seat: {
                        CanUpgrade: 'N', // 能否升舱
                        Type: "UPG",    // 类型，升舱
                        Choosed: "Y",// "是否选座":"Y",
                        Origin: {   //"原始座位"
                            Cabin: "Y",
                            SeatNo: "C1",
                            Status: "",
                        },
                        Wanted: {   //"升舱座位"
                            Cabin: "Y",
                            SeatNo: "C1",
                            Status: "FRO",
                        }
                    },
                    Order: {
                        OrderType: "GatewayUpgrade", //"订单类型":"GatewayUpgrade",
                        CreateDatetime: new Date().getTime(),//"创建时间":"",
                        OrderNo: "ORD19080900024708", //"订单编号":"",
                        ChannelOrderNo: "", //"渠道订单号": "",
                        Price: 50000,
                        Status: "PAYED",
                    },
                    Pay: {
                        Method: "WEIXIN", //支付方式
                        PayAt: new Date().getTime(), //支付时间
                        CountdownTimestamp: 3600, //支付剩余倒计时的时间戳
                        Payed: "N", //是否支付
                        Currency: "CNY", //币种
                    },
                    Remark: "--",
                    UnmanageableReason: "--",
                }
            ]

          }
          
        } catch (error) {
          ctx.body = {
            ResultCode: "10001",
            ErrorMsg: error.message
          }
        }
      })

      router.post('/faker/GateUpgrade/QueryUpgSeatMap/v10', async ctx => {
        try {          
            const body = ctx.request.body;
            const {
                Version = "30",
                ChannelCode = "B2C",
                UserNo = "10001",
                Ip = '127.0.0.1',
                Request = {}, 
              } = body;
  
            const {
              FlightNo,
              FlightDate,
              DepAirportCode,
              ArrAirportCode,
              Currency = 'CNY',
            } = Request;
  
            debug('Request Info: %O', Request);
            ctx.body = {
              Version,
              ChannelCode,
              UserNo,
              ServieCode: "-1",
              ResultCode: "1001",
              ErrorMsg: "OK",
              Result: {
                  FlightNo,
                  Cabin: 'A',
                  FlightDate,
                  DepAirportCode,
                  ArrAirportCode,
                  PlaneType: '空客A321(中)',
                  SeatMaplist: SeatMapData
              }
  
            }
            
          } catch (error) {
            ctx.body = {
              ResultCode: "10001",
              ErrorMsg: error.message
            }
          }
      })

      router.post('/faker/GateUpgrade/CreateOrder/v10', async ctx => {
        try {          
            const body = ctx.request.body;
            const {
                Version = "30",
                ChannelCode = "B2C",
                UserNo = "10001",
                Ip = '127.0.0.1',
                Request = {}, 
              } = body;
  
            const {
                FlightNo,
                FlightDate,
                DepAirportCode,
                ArrAirportCode,
                PsgName,
                PhoneNo,
                FfpId,
                FfpCardNo,
                CertNo,
                TktNo,
                PnrNo,
                Cabin,
                SeatNo,
                OriginCabin,
                OriginSeatNo,
                OrderNo,
                Price,
                Currency = 'CNY',
                UpgType
            } = Request;
  
            debug('Request Info: %O', Request);
            ctx.body = {
              Version,
              ChannelCode,
              UserNo,
              ServieCode: "-1",
              ResultCode: "1001",
              ErrorMsg: "OK",
              Result: {
                Order: {
                    OrderType: "GatewayUpgrade", //"订单类型":"GatewayUpgrade",
                    CreateDatetime: new Date().getTime(),//"创建时间":"",
                    OrderNo: "ORD19080900024708", //"订单编号":"",
                    ChannelOrderNo: "", //"渠道订单号": "",
                    Price: 50000,
                    Status: "UnPay",
                },
              }
  
            }
            
          } catch (error) {
            ctx.body = {
              ResultCode: "10001",
              ErrorMsg: error.message
            }
          }
      })
      
      router.post('/faker/GateUpgrade/GetOrderInfo/v10', async ctx => {
        try {          
          const body = ctx.request.body;
          const {
              Version = "30",
              ChannelCode = "B2C",
              UserNo = "10001",
              Ip = '127.0.0.1',
              Request = {}, 
            } = body;

          const {
            CertNo,
            PsgName,
            OrderNo,
          } = Request;

          debug('Request Info: %O', Request);
          ctx.body = {
            Version,
            ChannelCode,
            UserNo,
            ResultCode: "1001",
            ErrorMsg: "OK",
            Result:
                {
                    Flight: {
                        FlightNo: "HO1073",
                        FlightDate: "2019-10-07",
                        PlaneType: "空客A321(中)",
                        Currency: "CNY",
                        Depart: {
                            AirportCode: "SHA",
                            City: "上海",
                            Terminal: "浦东T2",
                            Time: "07:55"
                        },
                        Arrive: {
                            AirportCode: "WUH",
                            City: "武汉",
                            Terminal: "天河T3",
                            Time: "10:05"
                        }
                    },
                    Passenger:{
                        PsgName, // 姓名
                        CertNo, //ID
                        TktNo: "0181156006646", //票号
                        PnrNo: "pnr" // pnrno
                    },
                    Seat: {
                        CanUpgrade: 'N', // 能否升舱
                        Type: "UPG",    // 类型，升舱
                        Choosed: "Y",// "是否选座":"Y",
                        Origin: {   //"原始座位"
                            Cabin: "Y",
                            SeatNo: "C1",
                            Status: "",
                        },
                        Wanted: {   //"升舱座位"
                            Cabin: "Y",
                            SeatNo: "C1",
                            Status: "FRO",
                        }
                    },
                    Order: {
                        OrderType: "GatewayUpgrade", //"订单类型":"GatewayUpgrade",
                        CreateDatetime: new Date().getTime(),//"创建时间":"",
                        OrderNo, //"订单编号":"",
                        ChannelOrderNo: "", //"渠道订单号": "",
                        Price: 50000,
                        Status: "PAYED",
                    },
                    Pay: {
                        Method: "WEIXIN", //支付方式
                        PayAt: new Date().getTime(), //支付时间
                        CountdownTimestamp: 3600, //支付剩余倒计时的时间戳
                        Payed: "N", //是否支付
                        Currency: "CNY", //币种
                    },
                    Remark: "--",
                    UnmanageableReason: "--",
                }

          }
          
        } catch (error) {
          ctx.body = {
            ResultCode: "10001",
            ErrorMsg: error.message
          }
        }
      })

    fpm.bindRouter(router);
}