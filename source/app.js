'use strict';
const { Fpm } = require('yf-fpm-server');
const fpm = new Fpm();
const biz = fpm.createBiz('0.0.1');
const { init } = require('fpmc-jssdk');
const UpgradeGatewayFaker = require('./faker/upgrade_gateway');

const ServiceFaker = require('./faker/service_upgrade_gateway');

const { mock } = require('./middleware');

biz.addSubModules('test', {
    foo: async (args, ctx, before) => {
        return Promise.reject({errno: -3001})
    }
});
fpm.addBizModules(biz);
fpm.run()
    .then(() => {

        const { host = 'localhost', port = 9999 } = fpm.getConfig('server');
        init({ appkey:'123123', masterKey:'123123', endpoint: `http://${ host }:${ port }/api` });  

        UpgradeGatewayFaker(fpm);
        ServiceFaker(fpm);
        fpm.app.use(mock);
    });