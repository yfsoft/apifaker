const assert = require('assert');
const debug = require('debug')('TEST');
const axios = require('axios');

axios.defaults.baseURL = 'http://localhost:9999/';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

describe('Basic Service Test Unit', function(){
  before(function(done){
    done();
  })

  it('/upg/queryUpgTours()', async() => {
    try {
      const rsp = await axios.post('/upg/queryUpgTours', {});
      const { status, data } = rsp;
      debug(rsp);
      assert(status === 200, 'http status should be 200')
      assert(data.errcode === 0, 'errcode should be 0')
      
    } catch (error) {
      throw error;
    }
  })

  it('/Service/GateUpgrade/QueryUpgSeatMap/v10', async() => {
    try {
      const rsp = await axios.post('/Service/GateUpgrade/QueryUpgSeatMap/v10', {});
      const { status, data } = rsp;
      debug(rsp);
      assert(status === 200, 'http status should be 200')
      assert(data.errcode === 0, 'errcode should be 0')
      
    } catch (error) {
      throw error;
    }
  })
  
})