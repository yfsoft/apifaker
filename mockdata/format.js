const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const debug = require('debug');


const { SeatMapList } = require('./mockdata.json');

const newSeatMapList = _.map(SeatMapList, item => {
    const { Price } = item;
    if(!!Price) {
        // 包含 price 字段
        item.Price = Price.Price * 100
        item.Currency = 'CNY';
    }else{
        // 不包含该字段
        item.Price = 0;
        item.Currency = 'CNY';
    }

    return item;
})

fs.writeFile('./newdata.json',JSON.stringify(newSeatMapList), 'utf8',(error) => {
    console.log(error);
});