## 登机口升舱API 相关文档

### Test ENV:

BasicURL: `http://172.22.0.13:9212/`

### 相关数据结构

####  BaseRequestDTO

基本的请求报文结构

    |Name|Type|Description|Remark|Nullable|
    |-|-|-|-|-|
    |version|String|接口版本号|30|N|
    |channelCode|String|渠道代码|10000、20000、30000等|N|
    |userNo|String|用户MemberId| |Y|
    |ip|String|源请求者IP|可填写 127.0.0.1 |N|
    |request|<? extends DTO>|自定义的请求参数| |N|

####  BaseResultDTO

基本的请求返回信息

    |Name|Type|Description|Remark|Nullable|
    |-|-|-|-|-|
    |version|String|接口版本号|30|Y|
    |channelCode|String|渠道代码|10000、20000、30000等|Y|
    |userNo|String|用户MemberId| |Y|
    |servieCode|String|接口编码| |Y|
    |resultCode|String|返回结果编码|非1001为错误返回，错误原因查看msg |Y|
    |errorMsg|String|返回结果描述|正确 ok |Y|
    |result|<? extends DTO>|自定义的返回值类型| |Y|

#### UpgTourQueryRequestDTO

 查询旅客航班行程的请求报文数据

    |Name|Type|Description|Remark|Nullable|
    |-|-|-|-|-|
    |certNo|String|证件号| |N|
    |psgName|String|乘客姓名| |N|


#### UpgTourQueryResponseDTO

旅客行程

    |Name|Type|Description|Remark|Nullable|
    |-|-|-|-|-|
    |flightNo|	String|	航班号|	|	N|
    |flightDate|	String|	航班日期|	yyyy-MM-dd|	N|
    |depAirportCode|	String|	出发机场三字码|		|N|
    |arrAirportCode|	String|	到达机场三字码|		|Y|
    |depTerminal|	String|	出发机场航站楼|		|N|
    |arrTerminal|	String|	到达机场航站楼|		|Y|
    |depTime|	String|	计划起飞时间|	hhmm	|Y|
    |arrTime|	String|	计划到达时间|		|N|
    |planeType|	String|	机型|		|N|
    |psgName|	String|	姓名|		|N|
    |tktNo|	String|	票号|		|Y|
    |pnrNo|	String|	pnr|		|Y|
    |certNo|	String|	输入的证件号|		|Y|
    |originCabin|	String|	原舱位|		|N|
    |orginSeatNo|	String|	原座位号|		|Y|
    |cabin|	String|	升舱后舱位|		| |
    |seatNo|	String|	升舱后座位号|	| |	
    |type|	String|	行程类型（UPG-升舱，DNG-降舱）|		| |
    |status|	String|	状态 T-升舱成功记录，F-取消，FRO-冻结，UNFRO-解冻|		| |
    |orderNo|	String|	订单号|		| |
    |price|	String|	价格|		| |
    |currency|	String|	币种|		| |
    |remark|	String|		|	| |
    |canUpgrade|	boolean|	是否可升舱|		| |
    |unmanageableReason|	String|	不可升舱原因		| | |




#### UpgSeatMapRequestDTO

 查询升舱座位图的请求报文数据

    |Name|Type|Description|Remark|Nullable|
    |-|-|-|-|-|
    |flightNo|String|航班号| |N|
    |flightDate|String|航班日期| |N|
    |depAirportCode|String|出发机场三字码||N|
    |arrAirportCode|String|到达机场三字码||N|
    |currency|String|币种||N|

 #### FlightSeatMapResponseDTO

 查询升舱座位图的返回数据

    |Name|Type|Description|Remark|Nullable|
    |-|-|-|-|-|  
    |flightNo|	String	航班号| | |
    |cabin	|String	舱位| | |
    |flightDate	|String	|航班日期| | |
    |depAirportCode|	String	|出发机场三字码| | |
    |arrAirportCode|	String	|到达机场三字码| | |
    |planeType|	String	|机型| | |
    |seatMaplist|	List<SeatChartDTO>|	座位图| | |
 
 #### SeatChartDTO

 座位信息的数据结构

    |Name|Type|Description|Remark|Nullable|
    |-|-|-|-|-|  
    |seatColumn|	char|	列| | |
    |seatRow|	int|	排| | |
    |seatStatus|	char|	座位状态（具体见下表）| | |
    |side|	char|	L-左边,R-右边| | |
    |cabin|	String|	舱位| | |
    |price|	String|	价格| | |
    |currency|	String|	币种 | | |


#### SeatStateTable

座位状态对比表（对于当前业务，进*可选）

    |状态|说明|
    |-|-|
    |.	|已经有旅客占用|
    |:	|此半行有婴儿|
    |/	|靠背不可移动的座位|
    |E	|此行或半行有紧急出口|
    |Q	|此行是安静座位|
    |T	|转港占用（锁定）区|
    |D	|VIP 留座|
    |P	|为未到旅客保留的座位|
    |O	|为其它航段保留的座位|
    |B	|可利用的摇篮座位|
    |U	|可利用的无人陪伴座位|
    |H	|头上宽敞的座位|
    |L	|脚下空间宽敞的座位(宽松)|
    |*	|可利用的座位|
    |=	|过道|
    |+	|为婴儿预留座位|
    |I	|此行婴儿优先|
    |X	|锁定（不可利用）座位|
    |V	|ASR订座名单中保留座|
    |C	|最后可利用座位，*用完后才可用|
    |R	|团体留座，系统自动给9人以上团留座|
    |A	|为本段保留的座位|
    |N	|看不到电影的座位|
    |G	|RS指令保留的团体座位|
    |> 	|在本航站转港的旅客占用的座位|


#### UpgRequestDTO

旅客升舱/解冻/冻结操作 的请求报文结构

    |Name|Type|Description|Remark|Nullable|
    |-|-|-|-|-|  
    |flightNo|	String|	航班号		|	|	N|
    |flightDate|	String|	航班日期		|	|	N|
    |depAirportCode|	String|	出发机场三字码		|	|	N|
    |arrAirportCode|	String|	到达机场三字码		|	|	N|
    |psgName|	String|	旅客姓名		|	|	N|
    |phoneNo|	String|	手机号		|	|	N|
    |ffpId|	String|	会员id		|	|	N|
    |ffpCardNo|	String|	会员卡号		|	|	N|
    |certNo|	String|	证件号		|	|	N|
    |tktNo|	String|	票号		|	|	N|
    |pnrNo|	String|	pnr		|	|	N|
    |cabin|	String|	升舱舱位		|	|	N|
    |seatNo|	String|	升舱座位		|	|	N|
    |originCabin|	String|	原始舱位		|	|	N|
    |originSeatNo|	String|	原始座位		|	|	N|
    |orderNo|	String|	订单号		|	|	N|
    |price|	String|	价格		|	|	N|
    |currency|	String|	币种		|	|	N|
    |upgType|	String|	CONFIRM为确认升舱，UNFREEZE-解冻，FREEZE-冻结|	|	N|


#### UpgResponseDTO

旅客行程

    |Name|Type|Description|Remark|Nullable|
    |-|-|-|-|-|
    |flightNo|	String|	航班号|	|	N|
    |flightDate|	String|	航班日期|	yyyy-MM-dd|	N|
    |depAirportCode|	String|	出发机场三字码|		|N|
    |arrAirportCode|	String|	到达机场三字码|		|Y|
    |depTerminal|	String|	出发机场航站楼|		|N|
    |arrTerminal|	String|	到达机场航站楼|		|Y|
    |depTime|	String|	计划起飞时间|	hhmm	|Y|
    |arrTime|	String|	计划到达时间|		|N|
    |planeType|	String|	机型|		|N|
    |psgName|	String|	姓名|		|N|
    |tktNo|	String|	票号|		|Y|
    |pnrNo|	String|	pnr|		|Y|
    |certNo|	String|	输入的证件号|		|Y|
    |originCabin|	String|	原舱位|		|N|
    |orginSeatNo|	String|	原座位号|		|Y|
    |cabin|	String|	升舱后舱位|		| |
    |seatNo|	String|	升舱后座位号|	| |	
    |type|	String|	行程类型（UPG-升舱，DNG-降舱）|		| |
    |status|	String|	状态 T-升舱成功记录，F-取消，FRO-冻结，UNFRO-解冻|		| |
    |orderNo|	String|	订单号|		| |
    |price|	String|	价格|		| |
    |currency|	String|	币种|		| |
    |remark|	String|		|	| |



### 接口列表

1) 0.1 获取旅客航班行程信息 `/upg/queryUpgTours`

    具体的API地址： [#/api/33](http://172.22.0.22:3000/project/21/interface/api/33)

    - Request: `UpgTourQueryRequestDTO`

    ```javascript
    // Demo Request Body
    {
    "request": {
        "certNo": "aa",
        "psgName": "aa"
    }
    }
    ```

    - Response: `UpgTourQueryResponseDTO`

    ```javascript
    {
        "error": 1001,
        "msg": "SUCC",
        "total": 2,
        "rows": [
            {
                "航班信息": {
                    "时间": "", 
                    "航班号":"",
                    "机型":"",
                    "飞行时间":"",
                    "币种":"",
                    "出发": {
                        "时间":"",
                        "航站楼":"",
                        "城市":"",
                        "3字码":"",
                    },
                    "到达": {
                        "时间":"",
                        "航站楼":"",
                        "城市":"",
                        "3字码":"",
                    },
                }
                "旅客信息":{
                    "姓名":"",
                    "ID":"",
                    "票号":"",
                    
                },
                "座位信息":{
                    "原始座位": {
                        "舱位": "Y",
                        "座位号":"",
                    },
                    "升舱座位":{
                        "舱位": "Y",
                        "座位号":"",
                    }
                },
                "订单信息":{
                    "创建时间":"",
                    "订单编号":"",
                    "渠道订单号": "",
                },
                "支付信息":{
                    "支付方式":"",
                    "支付时间":"",
                    "是否支付":"",//
                    "金额":"",
                }
            }
        ]
    }
    ```


2) 0.2 查询升舱座位图 `/upg/queryUpgSeatMap`

    具体的API地址： [#/api/34](http://172.22.0.22:3000/project/21/interface/api/34)

    - Request: `UpgSeatMapRequestDTO`

    ```javascript
    // Demo Request Body
    {
        "request": {
            "flightNo": "HO1073",
            "flightDate": "2019-10-07",
            "depAirportCode": "SHA",
            "arrAirportCode": "WUH",
            "currency": "CNY"
        }
    }
    ```

    - Response: `FlightSeatMapResponseDTO`

    ```javascript
    {

    }
    ```


3) 0.3 旅客升舱/解冻/冻结操作 `/upg/passengerUpgrade`

    具体的API地址： [#/api/35](http://172.22.0.22:3000/project/21/interface/api/35)

    - Request: `UpgRequestDTO`

    ```javascript
    // Demo Request Body
    {
        "request": {
            "flightNo": "HO1073",
            "flightDate": "2019-10-07",
            "depAirportCode": "SHA",
            "arrAirportCode": "WUH",
           " psgName": "王帆",
            "phoneNo": "13770683580",
            "ffpId": "10001",
           "ffpCardNo": "2881639283",
            "certNo": "321088198801235454",
            "tktNo": "018-1156006646",
            "pnrNo": "",
            "cabin": "A",
            "seatNo": "A1",
            "originCabin": "Y",
            "originSeatNo": "Y2",
            "orderNo": "ORD19080900024708",
            "price": "500.00",
            "currency": "CNY",
            "upgType": "CONFIRM"
        }
    }
    ```

    - Response: `UpgResponseDTO`

    ```javascript
    {

    }
    ```
