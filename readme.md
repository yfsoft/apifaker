ISSUE:
- [ ] 容器需要运行一次 npm run install-server

    运行容器的 bash `docker exec -it yapi-app sh` 这里的 sh 很重要，通常我们是用 /bin/bash ，这里的 alpine 类型的容器只有 sh，然后执行 上述指令即可。


MOCK:
- 接入 mongodb ，存放数据
- 运行一个mongodb

Upload Docker Image:

```sh
# docker images | grep apifaker | awk '{print $3}'

docker save $(docker images | grep apifaker | awk '{print $3}') > apifaker.tar

scp ./apifaker.tar root@172.22.0.22:/home/docker/images

ssh root@172.22.0.22
```

```bash
cd /home/docker/images

docker load < apifaker.tar
docker images

# docker ps | grep apifaker | awk '{print $1}' 获取 container id
docker stop apifaker && docker rm apifaker
docker rmi apifaker:beta

docker tag ${image id} apifaker:beta
docker run \
  -p 9991:9999 \
  -v "/home/docker/config.mocker.js:/app/config.json" \
  -d \
  --name=apifaker \
  apifaker:beta 
```


```bash

# run the mongadmin
docker run \
  -p 1234:1234 \
  -d --name=adminmongo \
  -e TZ="Asia/Shanghai" \
  yfsoftcom/adminmongo
```

